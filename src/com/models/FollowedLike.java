package com.models;

public class FollowedLike {

	private String likerName;
	private String checkerName;
	private String placeName;

	public String getLikerName() {
		return likerName;
	}

	public void setLikerName(String likerName) {
		this.likerName = likerName;
	}

	public String getCheckerName() {
		return checkerName;
	}

	public void setCheckerName(String checkerName) {
		this.checkerName = checkerName;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

}
