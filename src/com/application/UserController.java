package com.application;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.framework.AllServiceListener;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.models.FollowedComment;
import com.models.FollowedLike;
import com.models.FollowerList;
import com.models.Place;
import com.models.PlaceList;
import com.models.UserCheckin;
import com.models.UserModel;

@Path("/")
public class UserController {

	public static final String BASE_URL = "http://localhost:8080/FCISquare/rest/";
	@Context
	HttpServletRequest request;

	@GET
	@Path("/index")
	public Viewable index(@Context HttpServletRequest request) {
		request.setAttribute("obj", new String("IT Works"));
		return new Viewable("/index.jsp", null);
	}

	@GET
	@Path("/login")
	@Produces(MediaType.TEXT_HTML)
	public Response loginPage() throws URISyntaxException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") != null) {

			return Response.ok(new Viewable("/home.jsp", getAllUsersCheckinsLikesComments())).build();
		}

		return Response.ok(new Viewable("/Login.jsp")).build();
	}

	// ********************************************************************************
	@GET
	@Path("/signUp")
	@Produces(MediaType.TEXT_HTML)
	public Response signUpPage() {
		return Response.ok(new Viewable("/Signup.jsp")).build();
	}


	// ***************************************************

	@GET
	@Path("/ShowLocation")
	@Produces(MediaType.TEXT_HTML)
	public Response showLocationPage() {
		return Response.ok(new Viewable("/ShowLocation.jsp")).build();
	}

	///////////// **********************************************
	@POST
	@Path("/userDetails")
	@Produces(MediaType.TEXT_HTML)
	public Map<String, UserModel> userDetails(@FormParam("follower_id") String followerId) {

		String serviceUrl = BASE_URL + "getUserDetails";

		String urlParameters = "follower_id=" + followerId;
		//System.out.println(followerId);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		UserModel user = new UserModel();
		
		user = new Gson().fromJson(retJson, UserModel.class);
	
		//System.out.println(user);
		Map<String, UserModel> map = new HashMap<String, UserModel>();
		
		map.put("user", user);		
		
		return  map ;
	}
   //*********************************************
	@POST
	@Path("/getUserDetails")
	@Produces(MediaType.TEXT_HTML)
	public Response getUserDetails(@FormParam("follower_id") String followerId) {
		
		return  Response.ok(new Viewable("/userProfile.jsp",userDetails(followerId))).build();


	}

	//////////////// ******************************************************
	@POST
	@Path("/doSavePlace")
	@Produces(MediaType.TEXT_HTML)
	public Response savePlace(@FormParam("placeId") String place_id) {
		String serviceUrl = BASE_URL + "savePlace";

		HttpSession session = request.getSession();
		String userId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// sig
		String urlParameters = "UserID=" + userId + "&placeId=" + place_id;
		//System.out.println(urlParameters);
		
		Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		 
    return Response.ok(new Viewable("/placeProfile.jsp",placeDetails(place_id))).build();
	}


	//***************************************************************
	@POST
	@Path("/unSavePlace")
	@Produces(MediaType.TEXT_HTML)
	public Response unSave(@FormParam("placeId")String place_id) { // elplace elly
																// ha3melo
																// unsave
		HttpSession session = request.getSession();
		String userId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// signin
		String serviceUrl = BASE_URL + "unSavePlace";

		String urlParameters = "UserID=" + userId + "&placeId=" + place_id;
		 //System.out.println(urlParameters);

		Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		return Response.ok(new Viewable("/placeProfile.jsp",placeDetails(place_id))).build();
	}

	/// ************************************************************************************/
	@GET
	@Path("/checkInPlace")
	@Produces(MediaType.TEXT_HTML)
	public Response checkInPlace() {
		return Response.ok(new Viewable("/ShowLocation.jsp")).build();
	}
	
	/////// **********************************************************************************/
	@GET
	@Path("/GetAllNotifications")
	@Produces(MediaType.TEXT_HTML)
	public Response AllNotifications() {
		return Response.ok(new Viewable("/home.jsp")).build();
	}

	/////// **********************************************************************************/
	

	/////// **********************************************************************************/
	@GET
	@Path("/Nolacupcake")
	@Produces(MediaType.TEXT_HTML)
	public Response v() {
		return Response.ok(new Viewable("/NolaCupCakes.jsp")).build();
	}

	// *******************************************************************************************************
	@POST
	@Path("/updateMyLocation")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateLocation(@FormParam("lat") String lat, @FormParam("long") String lon, @FormParam("pname") String pName) {
		HttpSession session = request.getSession();

		String id = String.valueOf(session.getAttribute("id"));

		String serviceUrl = BASE_URL + "updatePosition";

		String urlParameters = "id=" + id + "&lat=" + lat + "&long=" + lon + "&pname=" + pName ;
		//System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		Gson gson = new Gson();

		int status = Integer.parseInt(gson.fromJson(retJson, String.class));

		try {
			if (status == 1)
				return "Your location is updated";
			else
				return "A problem occured";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "A problem occured";

	}
	// **********************************************************************************************

	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_HTML)
	public Response logOut() {

		request.getSession().invalidate();
		return Response.ok(new Viewable("/Login.jsp")).build();
	}

	// *********************************************************************************************

	@POST
	@Path("/doLogin")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("email") String email, @FormParam("pass") String pass) {

		String serviceUrl = BASE_URL + "login";

		String urlParameters = "email=" + email + "&pass=" + pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		HttpSession session = request.getSession();

		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("lng", obj.get("lng"));
			session.setAttribute("pass", obj.get("pass"));

			return Response.ok(new Viewable("/home.jsp", getAllUsersCheckinsLikesComments())).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	///////////////////////////////
	private static java.net.URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/FCISquareApp").build();
	}

	// ******************************************************************************************

	@POST
	@Path("/doSignUp")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("name") String name, @FormParam("email") String email,
			@FormParam("pass") String pass) {
		// String serviceUrl =
		// "http://se2firstapp-softwareeng2.rhcloud.com/FCISquare/rest/signup";
		// String serviceUrl =
		// "http://firstapplication-fciteamproject.rhcloud.com/FCISquare/rest/signup";

		String serviceUrl = BASE_URL + "signup";

		String urlParameters = "name=" + name + "&email=" + email + "&pass=" + pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("name"));
			map.put("email", (String) obj.get("email"));

			return Response.ok(new Viewable("/home.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	// *************************************************************************************************

	@POST
	@Path("/doFollowUser")
	@Produces(MediaType.TEXT_HTML)
	public Response FollowUser(@FormParam("UserID") String user_id) {
		
		String serviceUrl = BASE_URL + "followUser";

		HttpSession session = request.getSession();
		String followerId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// sig
		String urlParameters = "FollowerID=" + followerId + "&UserID=" + user_id;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
	
			return Response.ok(new Viewable("/home.jsp", getAllUsersCheckinsLikesComments())).build();


	}

	//

	// **********************************************************************************************
	@POST
	@Path("/UnFollow")
	@Produces(MediaType.TEXT_HTML)
	public Response UnFollow(@FormParam("UserID") int userId) { // eluser elly
																// ha3melo
																// unfollow

		HttpSession session = request.getSession();
		String followerId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// signin
		String serviceUrl = BASE_URL + "unfollowUser";

		String urlParameters = "UserID=" + userId + "&FollowerID=" + followerId;
		// System.out.println(urlParameters);

		Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		return Response.ok(new Viewable("/home.jsp", getAllUsersCheckinsLikesComments())).build();
	}
	// ******************************************************************************************
	// ******************************************************************************************

	@GET
	@Path("/getAllUsers")
	// @Produces(MediaType.TEXT_HTML)
	public String getAllUsers() {
		String serviceUrl = BASE_URL + "getAllUsers";

		String retJson = Connection.connect(serviceUrl, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<UserModel> getUsers(String jsonResult) {

		try {
			Type type = new TypeToken<List<UserModel>>() {
			}.getType();
			List<UserModel> usersList = new Gson().fromJson(jsonResult, type);
			return usersList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	
	
	// ***************************************************************

	public Map<String, Object> getAllUsersCheckinsLikesComments() {

		Map<String, Object> map = new HashMap<String, Object>();

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget checkinService = client.target(getBaseURI());
		WebTarget likeService = client.target(getBaseURI());
		WebTarget commentService = client.target(getBaseURI());
		WebTarget usersService = client.target(getBaseURI());

		String userJson = usersService.path("app").path("getAllUsers").request().get(String.class);
		String checkinJson = checkinService.path("app").path("showUserCheckin").request().get(String.class);
		String likeJson = likeService.path("app").path("showLikedList").request().get(String.class);
		String commentJson = commentService.path("app").path("showCommentList").request().get(String.class);

		map.put("users", getUsers(userJson));
		map.put("checkins", getUserCheckins(checkinJson));
		map.put("likes", getFollowedLike(likeJson));
		map.put("comments", getFollowedComment(commentJson));

		return map;
	}

	// ***************************************************************
	@GET
	@Path("/showUserCheckin")
	@Produces(MediaType.TEXT_HTML)
	public String showUserCheckin() {
	

		String serviceUrl = BASE_URL + "showUserCheckin";

		String urlParameters = "UserID=" + 10;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<UserCheckin> getUserCheckins(String jsonResult) {

		try {

			Type type = new TypeToken<List<UserCheckin>>() {
			}.getType();

			List<UserCheckin> userCheckinsList = new Gson().fromJson(jsonResult, type);

			return userCheckinsList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	// el7agat elly elusers elly 3amelha follow 3malet 3aleha like
	@GET
	@Path("/showLikedList")
	@Produces(MediaType.TEXT_HTML)
	public String showLikedList() {

		String serviceUrl = BASE_URL + "showLikeList";

		String urlParameters = "UserID=" + 10;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<FollowedLike> getFollowedLike(String jsonResult) {

		try {

			Type type = new TypeToken<List<FollowedLike>>() {
			}.getType();

			List<FollowedLike> followedUsersLikesList = new Gson().fromJson(jsonResult, type);

			return followedUsersLikesList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	// el7agat elly elusers elly 3amelha follow 3malo comment 3aleha
	@GET
	@Path("/showCommentList")
	@Produces(MediaType.TEXT_HTML)
	public String showCommentList() {
		// String serviceUrl =
		// "http://se2firstapp-softwareeng2.rhcloud.com/FCISquare/rest/login";
		// String serviceUrl =
		// "http://firstapplication-fciteamproject.rhcloud.com/FCISquare/rest/login";

		String serviceUrl = BASE_URL + "showCommentList";

		String urlParameters = "UserID=" + 10;

		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<FollowedComment> getFollowedComment(String jsonResult) {

		try {

			Type type = new TypeToken<List<FollowedComment>>() {
			}.getType();

			List<FollowedComment> followedUsersCommentsList = new Gson().fromJson(jsonResult, type);

			return followedUsersCommentsList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/////// **************************************
	@POST
	@Path("/makeLike")
	@Produces(MediaType.TEXT_HTML)
	public void makeLike(@FormParam("checkId") int checkId) {
		// String serviceUrl =
		// "http://se2firstapp-softwareeng2.rhcloud.com/FCISquare/rest/login";
		// String serviceUrl =
		// "http://firstapplication-fciteamproject.rhcloud.com/FCISquare/rest/login";

		String serviceUrl = BASE_URL + "makeLike";

		HttpSession session = request.getSession();

		String id = String.valueOf(session.getAttribute("id"));

		String urlParameters = "checkId=" + checkId + "&userId=" + id;

		Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

	}

	/////// **************************************
	@POST
	@Path("/makeComment")
	@Produces(MediaType.TEXT_HTML)
	public void makeComment(@FormParam("checkId") int checkId, @FormParam("commentText") String comment) {
		// String serviceUrl =
		// "http://se2firstapp-softwareeng2.rhcloud.com/FCISquare/rest/login";
		// String serviceUrl =
		// "http://firstapplication-fciteamproject.rhcloud.com/FCISquare/rest/login";

		String serviceUrl = BASE_URL + "makeComment";

		HttpSession session = request.getSession();

		String id = String.valueOf(session.getAttribute("id"));

		String urlParameters = "checkId=" + checkId + "&userId=" + id + "&comment=" + comment;

		Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

	}

	/// *******************************************
	@POST
	@Path("/checkIn")
	@Produces(MediaType.TEXT_HTML)
	public Response checkIn(@FormParam("place_name") String place_name, @FormParam("latt") String lat,
			@FormParam("lnng") String lng) {

		Map<String, String> data = new HashMap<String, String>();

		String serviceUrl = BASE_URL + "checkIn";
		HttpSession session = request.getSession();
		String id = String.valueOf(session.getAttribute("id"));

		Date dateNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat timee = new SimpleDateFormat("hh:mm:ss");

		System.out.println(lat + " " + lng);
		data.put("name", place_name);
		data.put("lat", lat);
		data.put("lng", lng);

		String urlParameters = "place_name=" + place_name + "&userId=" + id + "&date=" + ft.format(dateNow) + "&time="
				+ timee.format(dateNow);
		String result = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		if (Boolean.parseBoolean(result) == false) {
			return Response.ok(new Viewable("/addplace.jsp", data)).build();
		}
		return Response.ok(new Viewable("/home.jsp",getAllUsersCheckinsLikesComments())).build();
	}

	/// *******************************************
	@POST
	@Path("/addNewPlace")
	@Produces(MediaType.TEXT_HTML)
	public Response addNewPlace(@FormParam("place_name") String place_name,
			@FormParam("place_desc") String place_description, @FormParam("place_lat") String lat,
			@FormParam("place_lng") String lng) {

		String serviceUrl = BASE_URL + "addNewPlace";

		HttpSession session = request.getSession();
		String id = String.valueOf(session.getAttribute("id"));

		Date dateNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat timee = new SimpleDateFormat("hh:mm:ss");

		String urlParameters = "place_name=" + place_name + "&userId=" + id + "&date=" + ft.format(dateNow) + "&time="
				+ timee.format(dateNow) + "&lat=" + lat + "&lng=" + lng + "&place_description=" + place_description;

		Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return Response.ok(new Viewable("/home.jsp",getAllUsersCheckinsLikesComments())).build();
	}
//************************************************************
	@GET
	@Path("/ExplorePlaces")
	@Produces(MediaType.TEXT_HTML)
	public Response ExplorePlaces() {

		Map<String, List<Place>> map = new HashMap<String, List<Place>>();
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String placeJson = service.path("app").path("placesList").request().get(String.class);
		
		map.put("places", getPlaces(placeJson));		

		return Response.ok(new Viewable("/ExplorePlaces.jsp", map)).build();
	}
	//****************************************************
	@GET
	@Path("/placesList")
	// @Produces(MediaType.TEXT_HTML)
	public String getPlaces() {

		String serviceUrl = BASE_URL + "placesList";

		String retJson = Connection.connect(serviceUrl, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<Place> getPlaces(String jsonResult) {

		try {

			Type type = new TypeToken<List<Place>>() {
			}.getType();

			List<Place> placesList = new Gson().fromJson(jsonResult, type);

			return placesList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	//*************************************
	@POST
	@Path("/placeDetails")
	@Produces(MediaType.TEXT_HTML)
	public Map<String, Place> placeDetails(@FormParam("place_id") String placeId) {

		String serviceUrl = BASE_URL + "getPlaceDetails";

		String urlParameters = "place_id=" +placeId;

		String retJson = Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		Place place = new Place();
		
		place = new Gson().fromJson(retJson, Place.class);
		
		Map<String, Place> map = new HashMap<String, Place>();
		
		map.put("place", place);		
		
		return  map ;
	}
   //*********************************************
	@POST
	@Path("/getPlaceDetails")
	@Produces(MediaType.TEXT_HTML)
	public Response getPlaceDetails(@FormParam("place_id") String placeId) {
		
		return  Response.ok(new Viewable("/placeProfile.jsp",placeDetails(placeId))).build();


	}
	//********************************************************
	@GET
	@Path("/savedplaces")
	@Produces(MediaType.TEXT_HTML)
	public Response savedPlaces() {
		
		Map<String, List<PlaceList>> map = new HashMap<String, List<PlaceList>>();
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String placeListJson = service.path("app").path("savedPlacesList").request().get(String.class);
		
		map.put("savedPlaces", savedPlaces(placeListJson));	
		
		return Response.ok(new Viewable("/savedplaces.jsp", map)).build();
	}
	//********************************************************
	
	@GET
	@Path("/savedPlacesList")
	public String getSavedPlaces() {
		
        String serviceUrl = BASE_URL + "savedPlacesList";
        //HttpSession session = request.getSession();
		//String userId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// sign in 
		//String urlParameters = "UserID=" + userId;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<PlaceList> savedPlaces(String jsonResult) {

		try {

			Type type = new TypeToken<List<PlaceList>>() {
			}.getType();

			List<PlaceList> savedPlacesList = new Gson().fromJson(jsonResult, type);
			//System.out.println("ay 7aggggggggggggggggga ");
			return savedPlacesList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	//*************************************************
	@GET
	@Path("/getFollowers")
	@Produces(MediaType.TEXT_HTML)
	public Response followerslist() {
		
		Map<String, List<FollowerList>> map = new HashMap<String, List<FollowerList>>();
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String followerListJson = service.path("app").path("followersList").request().get(String.class);
		
		map.put("followers", followlist(followerListJson));	
		
		return Response.ok(new Viewable("/getFollowers.jsp", map)).build();
	}
	//********************************************************
	
	@GET
	@Path("/followersList")
	public String getFollowers() {
		
        String serviceUrl = BASE_URL + "getFollowers";
        //HttpSession session = request.getSession();
		//String userId = String.valueOf(session.getAttribute("id")); // elragel
																		// elly
																		// 3amel
																		// sign in 
		//String urlParameters = "UserID=" + userId;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		return retJson;
	}

	public List<FollowerList> followlist(String jsonResult) {

		try {

			Type type = new TypeToken<List<FollowerList>>() {
			}.getType();

			List<FollowerList> followersList = new Gson().fromJson(jsonResult, type);
			//System.out.println("ay 7aggggggggggggggggga ");
			return followersList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	//*********************************************************
	
	@POST
	@Path("/getuserlastposition")
	@Produces(MediaType.TEXT_HTML)
	public Response getLastPosition(@FormParam("follower_id") String followerId) {

		String serviceUrl = BASE_URL + "getUserLastPosition";

		String urlParameters = "follower_id=" + followerId;
		System.out.println(followerId);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST", "application/x-www-form-urlencoded;charset=UTF-8");

		UserModel user = new UserModel();
		
		user = new Gson().fromJson(retJson, UserModel.class);
	
		Map<String, UserModel> map = new HashMap<String, UserModel>();
		
		map.put("position", user);		
		
		return Response.ok(new Viewable("/getuserposition.jsp", map)).build();
	}
   //*********************************************


}
	
	

