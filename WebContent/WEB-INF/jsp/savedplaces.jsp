<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>savedplaces</title>
<style>
a:visited {
	color: #FCFFF5;
	text-decoration: none;
	background-color: transparent;
}

a:hover {
	color: #D1DBBD;
	background-color: transparent;
	text-decoration: none;
}

.placeName {
    background-color: #193441; 
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
}
</style>
</head>
<body style="background-image: url(showcase-social-media-network.png)">
	<div
		style="background-color: #193441; color: white; height: 75px; width: 50%; margin-left: 25%; border-radius: 25px">

		<p
			style="font-family: courier; font-size: 200%; margin-left: 160px; padding-top: 20px">Saved
			places :)</p>
	</div>
	<div
		style="background-color: #193441; color: white; height: 600px; width: 50%; margin-left: 25%; border-radius: 25px; padding-top: 25px; margin-top: 15px">
		<table style="width: 100%; color: white;" border="1">
			<tr>
				<td>Place</td>
				<td style="margin-left: 100px">Category and description</td>
			</tr>

			<c:forEach var="place" items="${it.savedPlaces}">
 				<form action="getPlaceDetails" method="post">
					<tr style="height: 55px">
						 <input type="hidden" name="place_id"
							value='<c:out value="${place.placeId}"></c:out>' />
						<td><input type="submit" name="placeName" class="placeName" value="${place.placeName}"></td>
						<td>${place.placeDescription}</td>
					</tr>
 				</form>
			</c:forEach>
		</table>
	</div>
</body>
</html>