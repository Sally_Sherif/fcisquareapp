<html>
<head>
<title>addplace</title>
</head>
<body style="background-image: url(showcase-social-media-network.png);">
	<div
		style="height: 75px; width: 50%; background-color: #193441; border-radius: 25px; margin-left: 25%; color: white">
		<p
			style="font-family: courier; font-size: 150%; margin-left: 35%; padding-top: 20px">Add
			New Place</p>
	</div>
	<div
		style="height: 400px; width: 50%; background-color: #193441; border-radius: 25px; margin-left: 25%; color: white; margin-top: 20px; font-family: courier; font-size: 20px; padding-top: 10px">
		
		<form action="addNewPlace" method="post"
			style="margin-left: 50px; margin-top: 30px">
			<table>
				<tr>
					<td style="font-size: 22px; color: #ffffff">Place Name</td>
					<td><input type="text" name="place_name" value="${it.name}" placeholder="Write place name here."
						style="margin-left: 38px; width: 300px; height: 40px; border-radius: 3px"
						size="60" /></td>
				</tr>
				<tr>
					<td style="font-size: 22px; color: #ffffff">Place description</td>
					<td><input type="text" name="place_desc"  placeholder="Place Description"
						style="margin-left: 38px; width: 300px; height: 40px; border-radius: 3px"
						size="60" /></td>
				</tr>
				<tr>
					<td  style="font-size: 22px; color: #ffffff">latitude</td>
					<td><input type="text" name="place_lat" value="${it.lat}" placeholder="Latitude"
						style="margin-left: 38px; width: 300px; height: 40px;"/></td>
				</tr>
				<tr>
					<td style="font-size: 22px; color: #ffffff">Longitude</td>
					<td><input type="text" name="place_lng" value="${it.lng}" placeholder="Longtide"
						style="margin-left: 38px; width: 300px; height: 40px;"/></td>
				</tr>
			<td></td>
			
			<td><input type="submit" 
				style="width: 70px; height: 45px; border-radius: 7px; margin-top: 10px; margin-left: 40px"/></td>
					</table>
		
		</form>
	</div>
</body>
</html>