<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>home</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("#comment").click(function() {

			$('#comment').val('');
			return false;
		});
		$(".show-more").click(function() {
			if ($(".text").hasClass("show-more-height")) {
				$(this).text("View less comments");
			} else {
				$(this).text("View more comments");
			}

			$(".text").toggleClass("show-more-height");
		});

	});
</script>

<style>
a:visited {
	color: #FCFFF5;
	text-decoration: none;
	background-color: transparent;
}

a:hover {
	color: #D1DBBD;
	background-color: transparent;
	text-decoration: none;
}

.dropbtn {
	background-color: white;
	color: white;
	padding: 16px;
	font-size: 16px;
	border: none;
	cursor: pointer;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: white;
	min-width: 200px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	min-height: 40px;
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropdown-content a:hover {
	background-color: #f1f1f1
}

.dropdown:hover .dropdown-content {
	display: block;
}

.dropdown:hover .dropbtn {
	background-color: white;
}

#profile-description {
	width: 600px;
	margin-top: 10px;
	position: relative;
}

#profile-description .text {
	width: 580;
	margin-bottom: 5px;
	color: #193441;
	padding: 0 15px;
	position: relative;
	font-family: Arial;
	font-size: 20px;
	display: block;
}

#profile-description .show-more {
	color: #fff;
	position: relative;
	font-size: 15px;
	padding-top: 5px;
	height: 20px;
	text-align: left;
	cursor: pointer;
}

#profile-description .show-more:hover {
	color: #1779dd;
}

#profile-description .show-more-height {
	height: 62px;
	overflow: hidden;
}
</style>
</head>
<body style="background-color: white">

	<div
		style="background-color: #193441; height: 50px; border-radius: 25px; margin-top: 0px; clear: float">
		<ul style="diplay: inline-block; list-style-type: none">
			<li
				style="font-family: courier; font-size: 20px; margin-left: 15px; color: #D1DBBD; padding-top: 10px; float: left">
				Social Network</li>
			<li
				style="font-family: courier; font-size: 20px; margin-left: 40px; color: #D1DBBD; padding-top: 10px; float: left">
				<form action="" method="get">
					<input type="text" name="search_text" id="search_text"
						placeholder="   Search..." size="47"
						style="height: 30px; border-radius: 5px" />
					<button type="submit"
						style="margin-right: 30px; height: 30px; border-radius: 7px">search</button>
				</form>
			</li>
			<a href="logout"
				style="font-family: courier; font-size: 20px; margin-right: 30px; color: #D1DBBD; padding-top: 10px; float: right; text-decoration: none;">Log
				Out</a>
			<!-- 			<li style="font-family: courier; font-size: 20px; margin-right: 30px; color: #D1DBBD; padding-top: 10px; float: right">Log out</li>
 -->
		</ul>

		<li
			style="font-family: courier; font-size: 20px; margin-left: 150px; color: white; padding-top: 3px; float: left">
			<div class="dropdown">
				<button class="dropbtn"
					style="height: 5px; width: 3px; background-color: #193441">N</button>
				<div class="dropdown-content">
					<a href="notification1.html">Notify 1</a> <a
						href="notification2.html">Notify 2</a> <a
						href="notification3.html">Notify 3</a> <a
						href="notification4.html">Notify 4</a>
				</div>
			</div>
		</li>
		</ul>
	</div>
	<div style="margin-top: 10px; width: 140; height: 700px; float: left">
		<div
			style="width: 160px; height: 160px; background-color: #778F89; margin-left: 5px; border-radius: 50%">
			<img src="http://s32.postimg.org/xk7bsomfp/addphoto.png" alt="addyourphoto"
				style="width: 160px; height: 160px; border-radius: 50%">

		</div>
		<div
			style="width: 140px; height: 300px; background-color: #778F89; margin-top: 10px; margin-left: 15px; border-radius: 25px;">
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 29px; list-style-type: none"><a
				href="ShowLocation.html " style="text-decoration: none">location</a></li>
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 25px; list-style-type: none"><a
				href="getFollowers" style="text-decoration: none">Followers</a></li>
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 35px; list-style-type: none;"><a
				href="AddNewPlace" style="text-decoration: none; color: white">Add
					place</a></li>

			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 25px; list-style-type: none;"><a
				href="ShowLocation" style="text-decoration: none; color: white">CheckIn</a></li>
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 25px; list-style-type: none;"><a
				href="savedplaces" 
				style="text-decoration: none; color: white">Saved Places</a></li>
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 25px; list-style-type: none;"><a
				href="history.html" style="text-decoration: none; color: white">history
			</a></li>
			<li
				style="font-family: courier; font-size: 15px; padding-top: 20px; float: left; margin-left: 7px; list-style-type: none;"><a
				href="ExplorePlaces"
				style="text-decoration: none; color: white">Explore places</a></li>

		</div>
	</div>

	<div style="float: left">
		<!-- 		<div
			style="width: 600px; height: 100px; background-color: #778F89; margin-top: 20px; margin-left: 100px; border-radius: 5px;">
			<form action="" method="post" style="margin-top: 30px">
				<textarea style="width: 594px; height: 30px; border-radius: 5px;"
					placeholder="Say what you feel...">		
				</textarea>
				<div class="dropdown">
					<input type="text" name="checkin"
						style="height: 2px; margin-left: 0px" placeholder="   checkin"
						size="67" class="dropbtn">
					<div class="dropdown-content">
						<a href="#">place 1</a> <a href="#">place 2</a> <a href="#">place
							3</a>
					</div>
				</div>
				<button type="submit"
					style="margin-left: 550px; height: 25px; border-radius: 7px; margin-top: 3px">post</button>
			</form>
		</div>
 -->
		<div
			style="width: 600px; height: 100%; background-color: #778F89; margin-top: 20px; margin-left: 100px; border-radius: 10px;">

			<table style="width: 100%; margin-top: 3px">

				<c:forEach var="checkin" items="${it.checkins}">
					<form action="makeLike" method="post">
						<tr>
							<td><p
									style="font-family: courier; color: #193441; font-size: 20px; margin-left: 10px; margin-top: 20px; margin-bottom: 10px;">${checkin.uName}
									CheckedIn ${checkin.pName} at ${checkin.time} ${checkin.date}</p></td>
						</tr>
						<tr>
							<td><button type="submit" name="checkId"
									value='<c:out value="${checkin.checkId}"></c:out>'
									style="margin-left: 10px; height: 25px;">Like</button></td>
						</tr>
					</form>

					<form action="makeComment" method="post">
						<tr>
							<input type="hidden" name="checkId"
								value='<c:out value="${checkin.checkId}"></c:out>' />
							<td><input type="text" name="commentText" id="comment"
								placeholder="Comment..."
								style="width: 500px; height: 40px; margin-left: 10px; margin-top: 0px;" /></td>
						</tr>
					</form>
					<tr>
						<td>
							<div id="profile-description">
								<div class="text show-more-height">
									<c:forEach var="cmnt" items="${checkin.commentsList}">

										<b style="color: #ffffff;">${cmnt.commenterName}</b> ${cmnt.comment} <br>
										<br>
									</c:forEach>
								</div>
								<div class="show-more">View more Comments</div>
							</div>
						</td>
					</tr>
				</c:forEach>

				<!--  ////////////////////////////////////////////////////////////////////// -->

				<c:forEach var="like" items="${it.likes}">
					<tr>
						<td><p
								style="font-family: courier; color: #193441; font-size: 20px; margin-left: 10px; margin-top: 20px; margin-bottom: 10px;">
								${like.likerName} Liked That ${like.checkerName} CheckedIn
								${like.placeName}</td>
					</tr>

				</c:forEach>

				<!--  ////////////////////////////////////////////////////////////////////// -->

				<c:forEach var="comment" items="${it.comments}">
					<tr>

						<td><p
								style="font-family: courier; color: #193441; font-size: 20px; margin-left: 10px; margin-top: 20px; margin-bottom: 10px;">
								${comment.commenterName} Commented in ${comment.checkerName}
								CheckedIn ${comment.placeName} ${comment.comment}</td>
					</tr>
				</c:forEach>

			</table>
		</div>
	</div>

	<div
		style="width: 350px; height: 300px; margin-top: 20px; margin-left: 900px; border-radius: 25px; background-color: #778F89">

		<table
			style="width: 300px; height: 300px; margin-top: 10px; margin-left: 10px;">

			<c:forEach var="user" items="${it.users}">
				<form action="doFollowUser" method="post">
					<tr>
						<td style="margin: 0px; padding: 0px;"><p
								style="font-family: courier; color: #193441; font-size: 20px; margin-left: 30px; margin-top: 0px; margin-bottom: 0px;">${user.name}</p></td>

						<td><button type="submit" name="UserID"
								value='<c:out value="${user.id}"></c:out>'
								style="margin-left: 30px; height: 25px; border-radius: 7px;">Follow</button>
						<td>
				</form>

				<form action="UnFollow" method="post">

					<td><button type="submit" name="UserID"
							value='<c:out value="${user.id}"></c:out>'
							style="margin-left: 20px; height: 25px; border-radius: 7px;">UnFollow</button></td>
				</form>
				</tr>
			</c:forEach>

		</table>

		<a href="getuserlastposition"
			style="margin-left: 80px; padding-top: 40px; font-family: courier; font-size: 16px; text-decoration: none;">Get
			User Last Position</a>
	</div>

	<!-- <div
		style="width: 350px; height: 100px; margin-top: 5px; margin-left: 900px; border-radius: 10px; background-color: #778F89">
		<form action="" method="post"
			style="margin-left: 25px; margin-top: 30px">
			User ID<input type="text" name="UserID" style="margin-left: 86px" />
			<br> CheckIn Post ID<input type="text" name="checkId"
				style="margin-left: 30px" /> <br>
			<button type="submit"
				style="margin-left: 90px; height: 30px; border-radius: 7px; margin-top: 5px">Like</button>
		</form>
	</div>
	<div
		style="width: 350px; height: 100px; margin-top: 5px; margin-left: 900px; border-radius: 10px; background-color: #778F89">
		<form action="" method="post"
			style="margin-left: 25px; margin-top: 30px">
			User ID<input type="text" name="UserID" style="margin-left: 86px" />
			<br> CheckIn Post ID<input type="text" name="checkId"
				style="margin-left: 30px" /> <br>
			<button type="submit"
				style="margin-left: 90px; height: 30px; border-radius: 7px; margin-top: 5px">
				Add Comment</button>
		</form>
	</div>
 -->

</body>
</html>


