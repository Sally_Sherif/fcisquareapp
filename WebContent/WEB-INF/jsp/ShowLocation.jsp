<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>Your Location</title>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript">
	// Determine support for Geolocation and get location or give error
	if (navigator.geolocation) {
		navigator.geolocation
				.getCurrentPosition(displayPosition, errorFunction);
	} else {
		alert('It seems like Geolocation, which is required for this page, is not enabled in your browser. Please use a browser which supports it.');
	}

	// Success callback function
	function displayPosition(pos) {
		var mylat = pos.coords.latitude;
		var mylong = pos.coords.longitude;

		document.getElementById('lat').value = mylat;
		document.getElementById('long').value = mylong;

		//Load Google Map
		var latlng = new google.maps.LatLng(mylat, mylong);
		var myOptions = {
			zoom : 16,
			center : latlng,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(document.getElementById("googleMap"),
				myOptions);
		var infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.nearbySearch({
			location : latlng,
			radius : '500',
		}, callback);

		function callback(results, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				for (var i = 0; i < results.length; i++) {
					var place = results[i];

					createMarker(results[i]);
				}
			}
		}

		var geocoder = new google.maps.Geocoder;
		 
		geocoder.geocode({
			location : latlng
		}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
 
				var plc_id = results[0].place_id;
				 var req = new google.maps.places.PlacesService(map);
 
					req.getDetails({
						placeId : plc_id
					}, function(place, status) {
						if (status === google.maps.places.PlacesServiceStatus.OK) {
 
							document.getElementById('pname').value = place.name;
						}
					});
			}
		});
		
		var req = new google.maps.places.PlacesService(map);
		
		req.getDetails({
			 placeId: 'ahChIJ6eFf1NBGWBQR6U4KHoDVIk4'
		},function(place, status) {
		    if (status === google.maps.places.PlacesServiceStatus.OK) {
				document.getElementById('pname').value = place.name;
		    }});
		
		function createMarker(place) {

			var placeLoc = place.geometry.location;
			var marker = new google.maps.Marker({
				map : map,
				position : place.geometry.location
			});
			google.maps.event.addListener(marker, 'click', function() {
				document.getElementById('place_name').value = place.name;

				document.getElementById('latt').value = place.geometry.location
						.lat();
				document.getElementById('lnng').value = place.geometry.location
						.lng();

				infowindow.setContent('<div><strong>' + place.name
						+ '</strong><br>' + 'Place ID: ' + place.place_id
						+ '<br>' + place.formatted_address + '</div>');
				infowindow.open(map, this);
			});
		}

		var marker = new google.maps.Marker({
			position : latlng,
			map : map,
			title : "You're here"
		});

	}

	// Error callback function
	function errorFunction(pos) {
		alert('It seems like your browser or phone has blocked our access to viewing your location. Please enable this before trying again.');
	}
</script>
</head>
<body style="background-image: url(showcase-social-media-network.png)">
	<div
		style="background-color: #193441; color: white; height: 75px; width: 50%; margin-left: 25%; border-radius: 25px">

		<p
			style="font-family: courier; font-size: 150%; margin-left: 200px; padding-top: 20px">
			Your Location NoW</p>
	</div>

	<div id="googleMap"
		style="width: 700px; height: 340px; border-color: #193441; border-style: solid; margin-left: 325px; margin-top: 15px">
	</div>
	<br>
	<br>

	<div style="width: 700px; margin-left: 325px; margin-top: 15px;">
		<form action="checkIn" method="post">

			<input id="latt" type="hidden" name="latt" /> <input id="lnng"
				type="hidden" name="lnng" /> <input type="text" id="place_name"
				name="place_name" placeholder="Write Place Name..."
				style="width: 300px; height: 40px;" />
		</form>


		<form action="updateMyLocation" method="post">

			<input id="pname" type="hidden" name="pname" /> <input id="lat"
				type="hidden" name="lat" /> <input id="long" type="hidden"
				name="long" /> <input type="submit"
				value="update my current location " position
				style="background-color: #91AA9D; border-radius: 10%; width: 200px; height: 30px; margin-left: 580px" /><br>
		</form>
	</div>
</body>
</html>
