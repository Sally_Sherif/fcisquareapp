<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>Get Followers</title>
<style>
a:visited {
	color: #FCFFF5;
	text-decoration: none;
	background-color: transparent;
}

a:hover {
	color: #D1DBBD;
	background-color: transparent;
	text-decoration: none;
}

.placeName {
    background-color: #193441; 
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
}
</style>
</head>
<body style="background-image: url(showcase-social-media-network.png)">
	<div
		style="background-color: #193441; color: white; height: 75px; width: 50%; margin-left: 25%; border-radius: 25px">

		<p
			style="font-family: courier; font-size: 200%; margin-left: 160px; padding-top: 20px">Your
			Followers :)</p>
	</div>
	<div
		style="background-color: #193441; color: white; height: 600px; width: 50%; margin-left: 25%; border-radius: 25px; padding-top: 25px; margin-top: 15px">
		<table style="width: 100%; color: white;" border="1">
			<tr>
				<td>Name</td>
				<td style="margin-left: 100px">Email</td>
			</tr>

			<c:forEach var="follower" items="${it.followers}">
 				<form action="getUserDetails" method="post">
					<tr style="height: 55px">
						 <input type="hidden" name="follower_id"
							value='<c:out value="${follower.follower_id}"></c:out>' />
						<td><input type="submit" name="placeName" class="placeName" value="${follower.follower_name}"></td>
						<td>${follower.follower_email}</td>
					</tr>
 				</form>
			</c:forEach>
		</table>
	</div>
</body>
</html>