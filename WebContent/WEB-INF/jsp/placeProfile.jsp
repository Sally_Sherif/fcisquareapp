<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>Insert title here</title>
</head>
<body>

	<h3>${it.place.placeName}</h3>
	<h3>${it.place.placeDescription}</h3>
    <h3>${it.place.noOfSavedPlaces}</h3>
    <h3>${it.place.noOfCheckins}</h3>
    
	<form action="doSavePlace" method="post">
		<button type="submit" name="placeId"
			value='<c:out value="${it.place.placeId}"></c:out>'
			style="margin: 30px; height: 25px;width: 60px;">Save</button>
	</form>

	<form action="unSavePlace" method="post">

		<button type="submit" name="placeId"
			value='<c:out value="${it.place.placeId}"></c:out>'
			style="margin: 30px; height: 25px; width: 60px;">UnSave</button>
	</form>

</body>
</html>